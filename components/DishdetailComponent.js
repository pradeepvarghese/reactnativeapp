import React, { Component } from 'react';
import { Text, View, ScrollView, FlatList, Button, Modal, StyleSheet, Alert, PanResponder, Share } from 'react-native';
import { Card, Icon, Input, Rating } from 'react-native-elements';
import { connect } from 'react-redux';
import { baseUrl } from '../shared/baseUrl';
import { postFavorite, postComment } from '../redux/ActionCreators';
import * as Animatable from 'react-native-animatable';

const mapStateToProps = state => {
    return {
        dishes: state.dishes,
        comments: state.comments,
        favorites: state.favorites
        // rating: state.rating,
        // author: state.author,
        // comment: state.comment
    }
}


const mapDispatchToProps = dispatch => ({
    postFavorite: (dishId) => dispatch(postFavorite(dishId)),
    postComment: (dishId, rating, author, comment) => dispatch(postComment(dishId, rating, author, comment))
});

function RenderDish(props) {

    const dish = props.dish;

    handleViewRef = ref => this.view = ref;

    const recognizeDrag = ({moveX, moveY, dx, dy}) => {
        if(dx < -200)
            return true;
        else
            return false;
    };

    const panResponder = PanResponder.create({
        onStartShouldSetPanResponder: (e, gestureState) => {
            return true;
        },
        onPanResponderGrant: () => {
            this.view.rubberBand(1000)
                .then(endState=>console.log(endState.finished ? 'finished' : 'cancelled'));
        },
        onPanResponderEnd: (e, gestureState) => {
            if(recognizeDrag(gestureState)) {
                Alert.alert(
                    'Add to favorite?',
                    'Are you sure you wish to add ' + dish.name + ' to favorites?',
                    [
                        {
                            text: 'Cancel',
                            onPress: () => console.log('cancel pressed'),
                            style: 'cancel'
                        },
                        {
                            text: 'OK',
                            onPress: ()=>props.favorite? console.log('Already Favorite'): props.onPress()
                        }
                    ],
                    {cancelable:false}
                );
                return true;
            }
        }
    });


    const shareDish = (title, message, url) => {
        Share.share({
            title: title,
            message: title + ': ' + message + ' ' + url,
            url: url
        },{
            dialogTitle: 'Share ' + title
        })
    }
    
        if (dish != null) {
            return(
                <Animatable.View animation="fadeInDown" duration={2000} delay={1000}
                    ref={this.handleViewRef}
                    {...panResponder.panHandlers}>
                    <Card
                    featuredTitle={dish.name}
                    image={{uri: baseUrl + dish.image}}>
                        <Text style={{margin: 10}}>
                            {dish.description}
                        </Text>
                        <View style={styles.formRow}>
                            <Icon raised reverse name={props.favorite ? 'heart': 'heart-o'} type='font-awesome' color='#f50'
                                onPress={()=>props.favorite? console.log('Already Favorite'): props.onPress()}>
                            </Icon>
                            <Icon raised reverse type='font-awesome' name='pencil' color='blue' 
                                onPress={()=>props.toggalModal1()}>
                            </Icon>
                            <Icon
                                raised
                                reverse
                                name='share'
                                type='font-awesome'
                                color='#51D2A8'
                                style={styles.cardItem}
                                onPress={() => shareDish(dish.name, dish.description, baseUrl + dish.image)} />
                        </View>
                    </Card>
                </Animatable.View>
            );
        }
        else {
            return(<View></View>);
        }
}

function RenderComments(props){
    const comments = props.comments;
            
    const renderCommentItem = ({item, index}) => {
        
        return (
            <ScrollView>
                <View key={index} style={{margin: 10}}>
                    <Text style={{fontSize: 14}}>{item.comment}</Text>
                    <Text style={{fontSize: 12}}>{item.rating} Stars</Text>
                    <Text style={{fontSize: 12}}>{'-- ' + item.author + ', ' + item.date} </Text>
                </View> 
                </ScrollView>
        );
    };
    
    return (
        <Animatable.View animation="fadeInUp" duration={2000} delay={1000}>
            <Card title='Comments' >
            <FlatList 
                data={comments}
                renderItem={renderCommentItem}
                keyExtractor={item => item.id.toString()}
                />
            </Card>
        </Animatable.View>
    );
}

class Dishdetail extends Component {

    constructor(props){
        super(props);

        this.state = {
            rating: 1,
            author: null,
            comment: null,
            showModal: false
        }

        this.toggalModal = this.toggalModal.bind(this);
        this.postYourComment = this.postYourComment.bind(this);
    }

    toggalModal(){
        //alert('this');
        this.setState({showModal: !this.state.showModal })
    }

    markFavorite(dishId) {
        //this.setState({favorites: this.state.favorites.concat(dishId)});
        this.props.postFavorite(dishId);
    }

    postYourComment(){
        const dishId = this.props.navigation.getParam('dishId', '');
        //alert(dishId + ' ' + this.state.rating + ' ' + this.state.author + ' ' + this.state.comment)
        this.props.postComment(dishId, this.state.rating, this.state.author, this.state.comment);
        this.toggalModal();
        this.forceUpdate();
    }

    static navigationOptions = ({
        title: 'Dish Details'
    });


    render () {
        const dishId = this.props.navigation.getParam('dishId', '')

        return(
                <ScrollView>
                    <RenderDish dish={this.props.dishes.dishes[+dishId]} 
                        favorite={this.props.favorites.some(el=>el===dishId)}
                        onPress={()=>this.markFavorite(dishId)}
                        toggalModal1={()=>this.toggalModal()}
                    />

                    <RenderComments comments={this.props.comments.comments.filter((comment) => comment.dishId === dishId)}
                    />
                    <View>
                        <Modal animationType={'slide'}
                                transparent={false}
                                visible={this.state.showModal}
                                onDismiss={()=>{this.toggalModal()}} 
                                onRequestClose={()=>{this.toggalModal()}}>
                                <View style={styles.modal}>
                                    <Rating type='star' 
                                        imageSize={50}
                                        count={5} 
                                        showRating
                                        defaultRating={1}
                                        startingValue={1}
                                        onFinishedRating={(rating)=>{this.setState({rating:rating})}} />
                                
                                    <Input
                                                placeholder = "Author"
                                                leftIcon={{ type: 'font-awesome', name: 'user-o' }}
                                                onChangeText={(value)=>{this.setState({author:value})}}
                                    />
                                    <Input 
                                                placeholder = "Comment"
                                                leftIcon={{ type: 'font-awesome', name: 'comment-o' }}
                                                onChangeText={(value)=>{this.setState({comment:value})}}
                                    />
                                
                                <Button type='submit' value='submit'
                                    onPress={ ()=>{this.postYourComment()}}
                                    color ='#512DA8'
                                    title='Submit'
                                    />
                                    <View style={styles.formRow}></View>
                                <Button 
                                    onPress={ ()=>{this.toggalModal()}  }
                                    color ='grey'
                                    title='Cancel'
                                    />
                                </View>
                        </Modal>
                    </View>
                </ScrollView>
            );
    }
}

const styles = StyleSheet.create({
    input: {
        margin: 15,
        height: 40,
        borderColor: '#7a42f4',
        borderWidth: 1
     },  
    formRow: {
      alignItems: 'center',
      justifyContent: 'center',
      flex: 1,
      flexDirection: 'row',
      margin: 10
    },
    formLabel: {
        fontSize: 18,
        flex: 2
    },
    formItem: {
        flex: 1
    },
    modal: {
        justifyContent: 'center',
        margin: 20
    },
    modalTitle: {
        fontSize: 24,
        fontWeight: 'bold',
        backgroundColor: '#512DA8',
        textAlign: 'center',
        color: 'white',
        marginBottom: 20
    },
    modalText: {
        fontSize: 18,
        margin: 10
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(Dishdetail);